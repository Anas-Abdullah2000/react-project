import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createForms } from 'react-redux-form';
import { Dishes } from './dishes'
import { Leaders } from './leaders';
import { Comments } from './comments';
import { Promotions } from './promotions';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { InitialFeedback } from './forms';

export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({ // map each of the reducers to one of the 4 properties
            dishes: Dishes, //the property dishes will be managed by the reducer Dishes
            comments: Comments, //the property comments will be managed by the reducer Comments
            promotions: Promotions, //the property promotions will be managed by the reducer Promotions
            leaders: Leaders, //the property leaders will be managed by the reducer Leaders
            ...createForms({
                feedback: InitialFeedback   
            })
        }),
        applyMiddleware(thunk,logger)
    );

    return store;
}