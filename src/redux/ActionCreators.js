import * as ActionTypes from './ActionTypes';


import { baseUrl } from '../shared/baseUrl';


//Action Creator
export const addComment = (comment) => ({
    type: ActionTypes.ADD_COMMENT,
    payload: comment
});
export const postComment = (dishId, rating, author, comment) => (dispatch) => {

    const newComment = {
        dishId: dishId,
        rating: rating,
        author: author,
        comment: comment
    };
    newComment.date = new Date().toISOString();
    
    return fetch(baseUrl + 'comments', {
        method: "POST",
        body: JSON.stringify(newComment),
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(response => dispatch(addComment(response)))
    .catch(error =>  { console.log('post comments', error.message); alert('Your comment could not be posted\nError: '+error.message); });
};

// thunk - this function returns a function
export const fetchDishes = () => (dispatch) => {
    dispatch(dishesLoading(true));

    return fetch(baseUrl + 'dishes')
    .then(response => {
        if(response.ok){ // If the response is ok i.e there is no error, the response will be returned to the next '.then' 
            return response; 
        }
        else{ // There is a response, but the response is an error
            var error = new Error('Error '+ response.status + ': '+ response.statusText); // response.status contains the status code for the response e.g. 200, 404
            //response.statusText contains error message
            error.response = response;
            throw error;
        }
    },
    error => { // If there is no response
        var errmess  = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(dishes => dispatch(addDishes(dishes)))
    .catch(error => dispatch(dishesFailed(error.message)));
}

// this function returns an action object
export const dishesLoading = () =>({
    type: ActionTypes.DISHES_LOADING
});

//this function returns action object
export const dishesFailed = (errmess) => ({
    type: ActionTypes.DISHES_FAILED,
    payload: errmess
});

// this function returns action object
export const addDishes = (dishes) => ({
    type: ActionTypes.ADD_DISHES,
    payload: dishes
});

// --------- For comments --------------

// thunk - this function returns a function
export const fetchComments = () => (dispatch) => {
    return fetch(baseUrl + 'comments')
    .then(response => {
        if(response.ok){ // If the response is ok i.e there is no error, the response will be returned to the next '.then' 
            return response; 
        }
        else{ // There is a response, but the response is an error
            var error = new Error('Error '+ response.status + ': '+ response.statusText); // response.status contains the status code for the response e.g. 200, 404
            //response.statusText contains error message
            error.response = response;
            throw error;
        }
    },
    error => { // If there is no response
        var errmess  = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(comments => dispatch(addComments(comments)))
    .catch(error => dispatch(commentsFailed(error.message)));
}

//this function returns action object
export const commentsFailed = (errmess) => ({
    type: ActionTypes.COMMENTS_FAILED,
    payload: errmess
});

// this function returns action object
export const addComments = (comments) => ({
    type: ActionTypes.ADD_COMMENTS,
    payload: comments
});

//---------- For Promos------------

// thunk - this function returns a function
export const fetchPromos = () => (dispatch) => {
    
    dispatch(promosLoading());

    return fetch(baseUrl + 'promotions')
    .then(response => {
        if(response.ok){ // If the response is ok i.e there is no error, the response will be returned to the next '.then' 
            return response; 
        }
        else{ // There is a response, but the response is an error
            var error = new Error('Error '+ response.status + ': '+ response.statusText); // response.status contains the status code for the response e.g. 200, 404
            //response.statusText contains error message
            error.response = response;
            throw error;
        }
    },
    error => { // If there is no response
        var errmess  = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(promos => dispatch(addPromos(promos)))
    .catch(error => dispatch(promosFailed(error.message)));
}

// this function returns an action object
export const promosLoading = () =>({
    type: ActionTypes.PROMOS_LOADING
});

//this function returns action object
export const promosFailed = (errmess) => ({
    type: ActionTypes.PROMOS_FAILED,
    payload: errmess
});

// this function returns action object
export const addPromos = (promos) => ({
    type: ActionTypes.ADD_PROMOS,
    payload: promos
});
//----------------------leaders---------------------



// thunk - this function returns a function
export const fetchLeaders = () => (dispatch) => {
    
    dispatch(leadersLoading());

    return fetch(baseUrl + 'leaders')
    .then(response => {
        if(response.ok){ // If the response is ok i.e there is no error, the response will be returned to the next '.then' 
            return response; 
        }
        else{ // There is a response, but the response is an error
            var error = new Error('Error '+ response.status + ': '+ response.statusText); // response.status contains the status code for the response e.g. 200, 404
            //response.statusText contains error message
            error.response = response;
            throw error;
        }
    },
    error => { // If there is no response
        var errmess  = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(leaders => dispatch(addLeaders(leaders)))
    .catch(error => dispatch(leadersFailed(error.message)));
}
// this function returns action object
export const addLeaders = (leaders) => ({
    type: ActionTypes.ADD_LEADERS,
    payload: leaders
});

export const leadersLoading = () =>({
    type: ActionTypes.LEADERS_LOADING
});

//this function returns action object
export const leadersFailed = (errmess) => ({
    type: ActionTypes.LEADERS_FAILED,
    payload: errmess
});
//-------------------------------------------------------------------------------------------


export const postFeedback = (firstname, lastname, telnum, email, agree, contactType, message) => (dispatch) => {

   const newFeedback = {
        firstname: firstname,
        lastname: lastname,
        telnum: telnum,
        email: email,
        agree: agree,
        contactType: contactType,
        message: message

    }; 
    newFeedback.date = new Date().toISOString();
    
    return fetch(baseUrl + 'feedback', {
        method: "POST",
        body: JSON.stringify(newFeedback),
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(feedback => alert('Thankyou for your feedback!'+ JSON.stringify(feedback)))
    //.then(response => dispatch(addComment(response)))
    .catch(error =>  { console.log('post feedback', error.message); alert('Your feedback could not be posted\nError: '+error.message); });
};
