import * as ActionTypes from './ActionTypes';

//exporting the Reducer function
export const Comments = (state = {errMess: null, comments: []}, action) => {
    switch (action.type) {
        case ActionTypes.ADD_COMMENTS:
            return {...state, errMess: null, comments: action.payload};
        case ActionTypes.COMMENTS_FAILED:
            return {...state, errMess: action.payload};
        case ActionTypes.ADD_COMMENT: //case where action.type = ActionTypes.ADD_COMMENT
            var comment = action.payload;
            return {...state, comments: state.comments.concat(comment)}; // concatenating to the previous state, so that the previous state is not modified. This creates a new state object that we then return.
        default:
            return state;
    }
}